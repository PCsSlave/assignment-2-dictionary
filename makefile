ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: all clean

all: program clean

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	rm *.o
