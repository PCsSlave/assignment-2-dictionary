%include "lib.inc"
%define DATA_QUAD_SIZE 8

global find_word

section .text

;Принимает указатель на нуль-терминированную строку и начало словаря.
;Функция проходится по словарю, если найдено переданное значение,
;вернёт его адрес, иначе вернёт 0.
find_word:
    xor rax, rax
    .loop:
        add rsi, DATA_QUAD_SIZE
        push rdi
        push rsi
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .find
        mov rsi, [rsi - DATA_QUAD_SIZE]
        cmp rsi, 0
        jne .loop
        ret
    .find:
        mov rax, rsi
        ret