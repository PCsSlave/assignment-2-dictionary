%include "words.inc"
%include "lib.inc"
%define BUFFER_MAX_SIZE 255
extern find_word

global _start

section .rodata
    buffer_error: db "Buffer error!", 10, 0
    not_found_key: db "Key not found!", 10, 0

section .bss
    buffer: resb BUFFER_MAX_SIZE

section .text
_start:
    ;input buffer
    mov rdi, buffer
    mov rsi, BUFFER_MAX_SIZE
    call read_word
    cmp rax, 0
    jne .find_key
    mov rdi, buffer_error
    jmp .error
    .find_key:
        mov rdi, rax
        mov rsi, first
        push rdx
        call find_word
        pop rdx
        cmp rax, 0
        jne .print_value
        mov rdi, not_found_key
    .error:
        call print_error
        mov rdi, 1
        jmp exit
    .print_value:
        add rax, rdx
        inc rax
        mov rdi, rax
        call print_string
        call print_newline
        xor rdi, rdi
        jmp exit
